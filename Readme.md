## Задачи

Были выбраны 2 и 3 задача из списка.

## Task2

Рабочая директория: **task2**

Все тесты проводились на ubuntu:20.04.

Перед запуском нужно выполнить комманду:

```bash
sudo -s
export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1
chown -R root:root ./task2
cd task2
```

Для запуска локальной разработки:

```bash
docker-compose -f docker-compose.local.yml build
docker-compose -f docker-compose.local.yml up
```

После чего можно менять локальные файлы, и приложение будет само обновляться в контейнере.

Например можно обновить файл: task2/pages/api/hello.ts

```bash
res.status(200).json({ name: 'John Doe' }) >> res.status(200).json({ name: 'John Doe Test' })
# И проверить изменения
curl localhost:3000/api/hello
```

Для запуска на проде:

```bash
docker-compose -f docker-compose.prod.yml build
docker-compose -f docker-compose.prod.yml up -d
```

## Task3

Рабочая директория: **task3**

Реализация была сделана в gitalb. В PagerDuty не получилось зарегестрироваться, поэтому реальных токенов нету. 